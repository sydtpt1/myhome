/**
 * Created by s.peixoto.tanikawa on 3/13/2017.
 */
import { Injectable } from '@angular/core';
import {FirebaseListObservable} from 'angularfire2';
import {FirebaseService} from "./firebase.service";
import {User} from "../model/user";

@Injectable()
export class UserService {
    public user:User = new User();
    userInfo: FirebaseListObservable<any[]>;
    constructor(public afService: FirebaseService) {
    }

    fetchUser(auth:any){
        this.userInfo = this.afService.af.database.list('users/'+auth.uid) as FirebaseListObservable<any[]>;
        return this.userInfo;

    }
}


