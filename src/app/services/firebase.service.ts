import { Injectable } from '@angular/core';
import {AuthMethods, AuthProviders, AngularFire} from "angularfire2";

@Injectable()
export class FirebaseService {
  constructor(public af: AngularFire) {}
  /**
   * Logs in the user
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  login(email:string,password:string): Promise<boolean>{
    var creds: any = { email: email, password: password };
    var res: Promise<boolean> = new Promise((resolve, reject) => {
      this.af.auth.login(creds,{provider: AuthProviders.Password, method: AuthMethods.Password} ).then(

        result => {
          resolve(result);
        }, error =>
        {
          resolve(error);
        }
      )
    });
    return res;
  }


  loginWithGoogle() {
    return this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    });
  }
  /**
   * Logs out the current user
   */
  logout() {
    console.log('logout service');
    return this.af.auth.logout();
  }
}
