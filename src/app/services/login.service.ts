/**
 * Created by s.peixoto.tanikawa on 3/11/2017.
 */
import { Injectable } from '@angular/core';
import { AuthProviders, AngularFireAuth, FirebaseAuthState, AuthMethods } from 'angularfire2';

@Injectable()
export class LoginService {
  private authState: FirebaseAuthState;


  constructor(public auth$: AngularFireAuth) {
      this.authState = auth$.getAuth();
      auth$.subscribe((state: FirebaseAuthState) => {
        console.log('status = '+ state||JSON);
        this.authState = state;
      });
    }

    get authenticated(): boolean {
       return this.authState !== null;
    }


  signInWithFacebook(): firebase.Promise<FirebaseAuthState> {
    return this.auth$.login({
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup
    });
  }

  login(email: string, password: string): Promise<boolean> {
    var creds: any = { email: email, password: password };
    var res: Promise<boolean> = new Promise((resolve, reject) => {
      this.auth$.login(creds,{provider: AuthProviders.Password, method: AuthMethods.Password} ).then(

        result => {
          resolve(result);
      }, error =>
        {
          resolve(error);
        }
      )
    });
    return res;
  }
}
