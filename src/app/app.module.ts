import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { Login} from '../pages/login/login.component';
import { HomePage } from '../pages/home/home';
import { LightsPage } from '../pages/lights/lights';


//providers
import {FirebaseService} from "./services/firebase.service";
import {UserService} from "./services/user.service";


// Import the AF2 Module
import {AngularFireModule, firebaseAuthConfig} from 'angularfire2';

// AF2 Settings
export const firebaseConfig = {
  apiKey: "AIzaSyBP_mz_ZT0Yv-5UNhFP54keFZ3_toPuL4g",
  authDomain: "myhome-2a06a.firebaseapp.com",
  databaseURL: "https://myhome-2a06a.firebaseio.com",
  storageBucket: "myhome-2a06a.appspot.com",
  messagingSenderId: "29170844595"
};

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    Login,
    HomePage,
    LightsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig,firebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    Login,
    HomePage,
    LightsPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseService,
    UserService
    ]
})
export class AppModule {}
