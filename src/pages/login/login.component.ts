/**
 * Created by s.peixoto.tanikawa on 3/11/2017.
 */
import {Component, OnInit} from '@angular/core';
import {NavController, LoadingController, Loading} from 'ionic-angular';
import {FirebaseService} from "../../app/services/firebase.service";
import {HomePage} from "../home/home";
import {UserService} from "../../app/services/user.service";

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class Login implements OnInit{
  loading:Loading;

  constructor(
    public navCtrl: NavController,
    private firebaseService:FirebaseService,
    private userService:UserService,
    public loadingCtrl: LoadingController
  ){
    this.loading = this.loadingCtrl.create({
      content: 'Loggin In!'
    });

  }

  ngOnInit(){
    this.firebaseService.af.auth.subscribe(
      (auth) => {
        if(auth != null) {
          this.loading.present();
          this.userService.user.uid = auth.uid;
          this.userService.fetchUser(auth).subscribe(
            userInfos =>{
              for (let item of userInfos) {
                if(item.$key === 'name'){
                  this.userService.user.name = item.$value;
                  continue;
                }
                if(item.$key === 'email'){
                  this.userService.user.email = item.$value;
                  continue;
                }
                if(item.$key === 'provider'){
                  this.userService.user.provider = item.$value;
                  continue;
                }
                if(item.$key === 'home'){
                  this.userService.user.home= item.$value;
                  continue;
                }
              }
              this.loading.dismiss();
              this.navCtrl.setRoot(HomePage);
          });
        }
      }
    );

  }

  login(email:string,password:string){
    //console.log(email +' - '+password);
    this.firebaseService.login(email,password).then(
      (res) => {
        console.log(res);

      }

      );
  }

}
