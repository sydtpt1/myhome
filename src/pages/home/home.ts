import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Page1} from "../page1/page1";
import {Page2} from "../page2/page2";
import {FirebaseService} from "../../app/services/firebase.service";
import {LightsPage} from "../lights/lights";
import {UserService} from "../../app/services/user.service";

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('content') nav: NavController;
  root:any = Page1;
  menuItens: Array<{title: string, component: any}>;
  teste:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,private afService: FirebaseService,private userService:UserService) {
    this.teste = this.userService.user.name;

    // used for an example of ngFor and navigation
    this.menuItens = [
      { title: 'Lights', component: LightsPage },
      { title: 'Page One', component: Page1 },
      { title: 'Page Two', component: Page2 }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  openPage(Page:any){
    this.nav.setRoot(Page.component);
  }

  logout(){
    this.afService.logout();
  }
}
